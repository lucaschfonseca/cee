#ifndef CEE_H
#define CEE_H

#include <vector>

#include "Constants.h"
#include "ClientPersistence.h"
#include "ExecutionResult.h"
#include "InvoicePersistence.h"
#include "ConsumptionItem.h"

class Cee
{

public:
	Cee();

	/**
	* Checks the number of parameters and direct to the function able to fulfill the user's request.
	*
	* @param argc the parameters count.
	* @param argv the parameters values.
	* @return the execution result.
	*/
	ExecutionResult execute(int argc, char** argv);

private:
	bool filesOpenedCorrectly;

	ClientPersistence fClient;
	InvoicePersistence fInvoice;

	ExecutionResult execute(const char* arg);

	ExecutionResult execute(const char* arg1, const char* arg2);

	ExecutionResult execute(const char* arg1, const char* arg2, const char* arg3);

	ExecutionResult consumerResearch(const string& arg1, const string& arg2 = "", const string& arg3 = "");

	/**
	* Calculates the energy consumption and the amount to be paid from the consumption data entered by the user and displays the result.
	*
	* @param clientNumber the client identifier number.
	* @param regarding The target month.
	* @param inputPath the path of the input data.
	* @return the execution result.
	*/
	ExecutionResult calculateConsumption(const string& clientNumber, const string& regarding, const string& inputPath);

	/**
	* Calculates monthly consumption and daily consumption in kwh of a month from a list of consumption values and the number
	* of days of the month. 
	*
	* @param consumptionItems the client identifier number.
	* @param days The number of days of the target month.
	* @return The calculated consumption of the month.
	*/
	MonthlyConsumption recoverConsumptionInfo(vector<ConsumptionItem>& consumptionItems, int days);

	/**
	* Imports invoices from an external file or directory to the persistence file.
	* Execution result messages will be displayed on console.
	*
	* @param path the absolute path of the invoice.
	* @return the execution result.
	*/
	ExecutionResult importInvoices(const string& path);

	/**
	* Converts invoices for a given path or file.
	* Execution result messages will be displayed on console
	*
	* @param path the absolute path of the invoice.
	* @return the execution result.
	*/
	vector<string> convertInvoice(const string& path, map<string, int>& convertError);

	/**
	* Display result of import on screen
	*
	* @param imported the successful imported invoices path list. 
	* @param alreadyImported the already imported invoices path list.
	* @param failures the import failures invoices path list.
	* @param conversionError the conversion errors invoices path list.
	*/
	void showImportResult(vector<string> imported, vector<string>alreadyImported,
		vector<string>failures, const map<string, int>& conversionError);

	/**
	* Display consumption history data from an invoice
	*
	* @param invoice the target invoice.
	*/
	void showConsumptionHitoryData(Invoice invoice);

	/**
	* Display consumption history data from in a period;
	*
	* @param invoices the invoices on period.
	* @param periodBegin.
	* @param periodEnd.
	*/
	void showPeriodicalConsumption(const vector<Invoice>& invoices, string periodBegin, string periodEnd);
};

#endif // !CEE_H