#include "InvoiceParser.h"
#include "StringUtils.h"

#include <regex>

bool InvoiceParser::parse(Invoice& invoice, vector<string> content)
{
	map <string, Token> foundTokens;

	Client& client = invoice.getClient();
	HomeAddress& address = client.getHomeAddress();

	if (content.empty())
		return false;

#pragma region Regex
	string exDecimal = "([0-9]*[.])?[0-9]+";
	string exName = "^.*?(?=N�)";
	string exCpf = "([0-9]){3}.([0-9]){3}.([0-9]){3}-([0-9]){2}";
	string exZipCode = "^.*?[0-9]{5}-[0-9]{3}";
	string exRegarding = "([a-zA-Z]){3}/[0-9]{4}";
	string exDoubleValue = "[0-9]+,[0-9]+";
	string exReadingDates = "([0-9]){2}/([0-9]){2} ([0-9]){2}/([0-9]){2} ([0-9]){2}/([0-9]){2}";
	string exReadingValues = "[0-9]+.[0-9]+";
	string exDate = "([0-9]){2}/([0-9]){2}/([0-9]){2}";
	string exDueDate = "([0-9]){2}/([0-9]){2}/([0-9]){4}";
	string exReadinDateValue = "([0-9]){2}/([0-9]){2}";
	string exCity = string(exZipCode).append(" .*,");
	string exState = ", ([a-zA-Z]){2}";

#pragma endregion

	map<string, Token> targets = {
		//no keys
		{NAME, Token("", exName, 0, 0)},
		{ZIP_CODE, Token("", exZipCode, 0, 0) },
		{CITY, Token("", exCity, 0, 0) },
		{STATE, Token("", exState, 0, 0) },

		//inline items
		{K_YELLOW_FLAG, Token(K_YELLOW_FLAG, exDoubleValue, 0, 0, true)},
		{K_RED_FLAG, Token(K_RED_FLAG, exDoubleValue, 0, 0, true)},
		{K_CPF, Token(K_CPF, exCpf, 0, 0)},
		{K_CONTRIBUTION_MUNICIPAL_STREET_LIGHTING, Token(K_CONTRIBUTION_MUNICIPAL_STREET_LIGHTING, exDoubleValue, 0, 0,  true)},

		// jump one line
		{K_CLIENT_NUMBER, Token(K_CLIENT_NUMBER, exDecimal, 1, 0)},
		{K_INSTALLATION_NUMBER, Token(K_INSTALLATION_NUMBER,exDecimal, 1, 1)},

		// jump 2 lines
		{K_DUE_DATE, Token(K_DUE_DATE, exDueDate, 2, 0)},
		{K_REGARDING, Token(K_REGARDING, exRegarding, 2, 0)},
		{K_CONSUMPTION_KWH, Token(K_CONSUMPTION_KWH, exDecimal, 2, 4)},

		{K_PREVIOUS_READING_DATE, Token(K_PREVIOUS_READING_DATE, exReadinDateValue, 2, 0)},
		{K_ACTUAL_READING_DATE, Token(K_ACTUAL_READING_DATE, exReadinDateValue,2, 1)},
		{K_NEXT_READING_DATE, Token(K_NEXT_READING_DATE, exReadinDateValue, 2, 2)},

		{K_PREVIOUS_READING, Token(K_PREVIOUS_READING, exDecimal, 2, 1)},
		{K_ACTUAL_READING, Token(K_ACTUAL_READING, exDecimal, 2, 2)},

		{K_AMOUNT_PAYABLE, Token(K_AMOUNT_PAYABLE, exDoubleValue, 2, 0)},
		{K_PRICE, Token(K_PRICE,exDoubleValue, 2, 0)},
		{K_CONSUMPTION_HISTORY, Token(K_CONSUMPTION_HISTORY,K_CONSUMPTION_HISTORY, 0, 0)}
	};

	foundTokens = fetchTokensFromContent(content, targets);

	// if there are required tokens that were not found in the content, it will not be possible to parse
	for (auto it = targets.begin(); it != targets.end(); it++)
	{
		if (!it->second.isOptional())
			return false;
	}

	for (auto it = foundTokens.begin(); it != foundTokens.end(); it++) {
		string key = it->first;
		Token parserToken = it->second;
		int line = parserToken.getLine();
		string comparator = parserToken.getComparator();

		auto allMatches = getAllMatchs(content.at(line), regex(comparator));

		if (allMatches.empty() || parserToken.getColumn() > allMatches.size())
			return false;

		auto value = allMatches[parserToken.getColumn()];

		if (key == ZIP_CODE) {
			address.setZipCode(value.substr(value.find_first_not_of(" ")));

			if (line - 4 >= 0) {
				string street = content.at(line - 4);
				address.setStreet(street.substr(street.find_first_not_of(" ")));
			}
			else
				return false;

			string neighborhood = content.at(line - 2);
			address.setNeighborhood(neighborhood.substr(neighborhood.find_first_not_of(" ")));

			continue;
		}

		if (key == CITY) {
			value = StringUtils::replaceAll(value, "  ", "-");
			value = value.substr(value.find_last_of(" "));
			value = value.substr(value.find_first_not_of(" "), value.find_first_of(",") - 1);

			address.setCity(value);
			continue;
		}

		if (key == STATE) {
			value = StringUtils::replaceAll(value, "  ", "-");
			value = value.substr(value.find_last_of(" "));
			value = value.substr(value.find_first_not_of(" "), value.find_first_of(",") - 1);

			address.setState(value);
			continue;
		}

		if (key == K_INSTALLATION_NUMBER) {
			invoice.setInstallationNumber(value);
			continue;
		}

		if (key == K_CLIENT_NUMBER) {
			client.setNumber(value);
			continue;
		}

		if (key == K_CPF) {
			client.setCpf(value);
			continue;
		}

		if (key == NAME) {
			client.setName(value.substr(value.find_first_not_of(" ")));
			continue;
		}

		if (key == NEIGHBORHOOD) {
			client.getHomeAddress().setNeighborhood(value);
			continue;
		}

		if (key == K_REGARDING) {
			invoice.setRegarding(value);
			continue;
		}

		if (key == K_YELLOW_FLAG) {
			invoice.setYellowFlag(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_RED_FLAG) {
			invoice.setRedFlag(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_DUE_DATE) {
			invoice.setDueDate(value);
			continue;
		}

		if (key == K_CONTRIBUTION_MUNICIPAL_STREET_LIGHTING) {
			invoice.setMunicipalLightingContribution(value);
			continue;
		}

		if (key == K_CONSUMPTION_KWH) {
			invoice.setKwhConsumption(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_PREVIOUS_READING) {
			invoice.setPreviousRead(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_ACTUAL_READING) {
			invoice.setActualRead(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_PREVIOUS_READING_DATE) {
			invoice.setPreviousReadingDate(value);
			continue;
		}

		if (key == K_ACTUAL_READING_DATE) {
			invoice.setActualReadingDate(value);
			continue;
		}

		if (key == K_NEXT_READING_DATE) {
			invoice.setNextReadingDate(value);
			continue;
		}

		if (key == K_AMOUNT_PAYABLE) {
			invoice.setTotalValue(StringUtils::toDouble(value));
			continue;
		}

		if (key == K_PRICE) {
			invoice.setElectricityPrice(StringUtils::toDouble(value));
			continue;
		}
	}

	auto it = foundTokens.find(K_CONSUMPTION_HISTORY);
	if (it != foundTokens.end()) {
		int lineNumber = it->second.getLine() + 2;

		if (!parseHistoric(invoice, content, lineNumber))
			return false;
	}
	else
		return false;

	return true;
}

map<string, Token> InvoiceParser::fetchTokensFromContent(vector<string> content, map<string, Token>& targets)
{
	map<string, Token> foundTokens;

	for (unsigned int i = 0; i < content.size() && !targets.empty(); i++) { // loops until read all content or find all tokens
		string s = string(content.at(i));

		if (s.empty())
			continue;

		for (auto it = targets.begin(); it != targets.end(); )
		{
			Token parserKey = it->second;
			string key = parserKey.getKey();

			if (key.empty()) {
				if (!(getAllMatchs(s, regex(parserKey.getComparator()))).empty())
				{
					parserKey.setLine(i + parserKey.getLinesForwward());
					foundTokens.insert(pair<string, Token>(it->first, parserKey));
					targets.erase(it++);
				}
				else
					++it;
			}
			else if (StringUtils::containsIgnoreCase(s, key))
			{
				parserKey.setLine(i + parserKey.getLinesForwward());
				foundTokens.insert(pair<string, Token>(key, parserKey));
				targets.erase(it++);
			}
			else
				++it;
		}
	}

	return foundTokens;
}

string getStringFromRegex(const string& str, regex r)
{
	std::smatch match;

	if (regex_search(str, match, r))
	{
		return match[0];
	}

	return "";
}

bool InvoiceParser::parseHistoric(Invoice& invoice, vector<string>& content, unsigned int beginsAt)
{
	unsigned int lineNumber = beginsAt;

	string exMonth = "[a-zA-Z]+";
	string exYear = "/[0-9]+ ";
	string exKwh = "[0-9]+ ";
	string exValue = "[0-9]+,[0-9]+";
	string exDays = " [0-9]+";

	string exp = string(exMonth).append(exYear).append(exKwh).append(exValue).append(exDays);

	regex r(exp);

	for (; lineNumber < content.size() - 1 && !invoice.isConsumptionEntryComplete(); lineNumber++) {
		string s = StringUtils::replaceAll(content.at(lineNumber), "  ", " ");
		const string result = getStringFromRegex(s, r);

		if (!result.empty()) {
			s = getStringFromRegex(result, regex(exYear));
			MonthlyConsumption c(getStringFromRegex(result, regex(exMonth)), StringUtils::toInt(s.substr(1)));

			s = getStringFromRegex(result, regex(exKwh + exValue));
			c.setKwh(StringUtils::toDouble(getStringFromRegex(s, regex(exKwh))));

			c.setAverageKWhPerDay(StringUtils::toDouble(getStringFromRegex(result, regex(exValue))));

			s = getStringFromRegex(result, regex(exValue + exDays));
			c.setDaysCount(StringUtils::toInt(getStringFromRegex(s, regex(exDays))));

			if (!invoice.addConsumptionEntry(c))
				break;
		}
	}

	return invoice.isConsumptionEntryComplete();
}