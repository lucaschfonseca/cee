#ifndef ELECTRICITY_ENERGY_CONSUMPTION_HOME_ADDRESS_H
#define ELECTRICITY_ENERGY_CONSUMPTION_HOME_ADDRESS_H

#include <string>

using namespace std;

class HomeAddress {
public:
	HomeAddress& setCity(string city);

	string getCity();

	HomeAddress& setZipCode(string zipCode);

	string getZipCode();

	HomeAddress& setNeighborhood(string neighborhood);

	string getNeighborhood();

	HomeAddress& setState(string state);

	string getState();

	HomeAddress& setStreet(string street);

	string getStreet();

	string toString();

private:
	string city;
	string zipCode;
	string state;
	string street;
	string neighborhood;
};

#endif //ELECTRICITY_ENERGY_CONSUMPTION_HOME_ADDRESS_H