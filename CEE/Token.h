#ifndef PARSER_KEY_H
#define PARSER_KEY_H

#include <string>


using namespace std;

class Token
{
	/**
	* Represents invoice tokens, with their keys and file positions
	*/

public:
	Token(string key, string comparator, unsigned int jumpLinesForward, unsigned int column, bool optional = false);

	string getKey();

	Token& setKey(string key);

	string getComparator();

	Token& setComparator(string comparator);

	unsigned int getLinesForwward();

	unsigned int getLine();

	Token& setLine(unsigned int line);

	unsigned int getColumn();

	Token& setColumn(unsigned int column);

	bool isOptional();

private:
	string key;
	string comparator;

	bool optional = false;

	unsigned int jumpLinesForwward = 0;
	unsigned int line = 0;
	unsigned int column = 0;
};

#endif // !PARSER_KEY_H