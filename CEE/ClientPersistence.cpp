#include "ClientPersistence.h"
#include "StringUtils.h"
#include "DerivedString.h"

ClientPersistence::ClientPersistence()
{
	initializeFile(F_CLIENT);
}

ClientPersistence::~ClientPersistence()
{
	if (file.is_open())
		file.close();
}

unsigned int ClientPersistence::countRegisters()
{
	return file.getSize() / sizeof(BinaryClient);
}

bool ClientPersistence::writeInto(Client obj)
{
	return file.writeInto(asStruct(obj));
}

bool ClientPersistence::readAt(Client& client, unsigned int position)
{
	BinaryClient obj;

	bool read = file.readFrom(obj, position);

	if (read)
		client = fromStruct(obj);

	return read;
}

vector<Client> ClientPersistence::readAll()
{
	vector<Client> all;

	for (BinaryClient c : file.readAll<BinaryClient>())
		all.emplace_back(fromStruct(c));

	return all;
}

bool ClientPersistence::removeAt(unsigned int position)
{
	return file.removeAt<BinaryClient>(position);
}

bool ClientPersistence::isGood()
{
	return file.is_open();
}

bool ClientPersistence::compareNumbers(Client client, string number)
{
	return client.getNumber() == number;
}

HomeAddress ClientPersistence::fromStruct(ClientPersistence::BinaryHomeAddress reg)
{
	HomeAddress address;

	address.setCity(string(reg.city));
	address.setZipCode(string(reg.zipCode));
	address.setState(string(reg.state));
	address.setStreet(string(reg.street));
	address.setNeighborhood(string(reg.neighborhood));

	return address;
}

ClientPersistence::BinaryHomeAddress ClientPersistence::asStruct(HomeAddress address)
{
	ClientPersistence::BinaryHomeAddress derived;

	StringUtils::toDerivedString(address.getCity(), derived.city);
	StringUtils::toDerivedString(address.getZipCode(), derived.zipCode);
	StringUtils::toDerivedString(address.getState(), derived.state);
	StringUtils::toDerivedString(address.getStreet(), derived.street);
	StringUtils::toDerivedString(address.getNeighborhood(), derived.neighborhood);

	return derived;
}

Client ClientPersistence::fromStruct(BinaryClient reg)
{
	Client client;

	client.setHomeAddress(fromStruct(reg.homeAddress));

	client.setName(string(reg.name));
	client.setCpf(string(reg.cpf));
	client.setNumber(string(reg.number));

	for (int i = 0; i < Client::MAXIMUM_INSTALLATIONS_PER_COSTUMER; i++) {
		string s = string(reg.installations[i]);

		if (s.empty())
			break;

		client.addInstallation(s);
	}

	return client;
}

ClientPersistence::BinaryClient ClientPersistence::asStruct(Client client)
{
	ClientPersistence::BinaryClient derived;
	unsigned int i;

	StringUtils::toDerivedString(client.getName(), derived.name);
	StringUtils::toDerivedString(client.getCpf(), derived.cpf);
	StringUtils::toDerivedString(client.getNumber(), derived.number);

	for (i = 0; i < client.getInstallations().size() && i <Client:: MAXIMUM_INSTALLATIONS_PER_COSTUMER; i++)
		StringUtils::toDerivedString(client.getInstallations().at(i), derived.installations[i]);

	for (; i < Client::MAXIMUM_INSTALLATIONS_PER_COSTUMER; i++)
		StringUtils::toDerivedString("", derived.installations[i]);

	derived.homeAddress = asStruct(client.getHomeAddress());

	return derived;
}