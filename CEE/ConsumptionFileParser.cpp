#include <regex>

#include "ConsumptionFileParser.h"
#include "StringUtils.h"
#include "Constants.h"

vector<ConsumptionItem> ConsumptionFileParser::parse(vector<string> content)
{
	vector<ConsumptionItem> data;
	vector<string> matches;

	regex rgxKwH("kWh");
	regex rgxDecimal("([0-9]*[,])?[0-9]+");

	for (string s : content) {
		ConsumptionItem consumption;

		if (StringUtils::startsWith(s, COMMENT_START_POINT))
			continue;

		matches = getAllMatchs(s, rgxDecimal);

		if (matches.empty())
			continue;

		if (matches.size() == 2) {
			consumption.setWatts(StringUtils::toDouble(matches.at(0)));
			consumption.setHours(StringUtils::toDouble(matches.at(1)));
		}
		else
			consumption.setKwh(StringUtils::toDouble(matches.at(0)));

		consumption.setDescription(s.substr(0, s.find_first_of(matches.at(0))));
		data.emplace_back(consumption);
	}

	return data;
}
