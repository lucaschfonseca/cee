#ifndef CONSUMPTION_FILE_PARSER_H
#define CONSUMPTION_FILE_PARSER_H

#include "ConsumptionItem.h"
#include "Parser.h"

class ConsumptionFileParser : Parser
{
public:

	/**
	* Parses the comsumption file into a vector ConsumptionItem of objects.
	*
	* @param content the source to be parsed.
	* @return the parsed items.
	*/
	vector<ConsumptionItem> parse(vector<string> content);
};

#endif // !CONSUMPTION_FILE_PARSER_H