#include "DirectoryUtils.h"
#include "StringUtils.h"


bool DirectoryUtils::exists(const string& path)
{
	return std::experimental::filesystem::exists(path);
}

bool DirectoryUtils::create(const string& path)
{
	std::experimental::filesystem::create_directories(path);

	return exists(path);
}

bool DirectoryUtils::createIfNotExists(const string& path)
{
	return exists(path) ? true : create(path);
}

int DirectoryUtils::getAllFiles(const string& path, vector<string>& files, const string& extension)
{
	int counter = 0;

	files.clear();

	if (exists(path)) {
		for (const auto& entry : std::experimental::filesystem::directory_iterator(path)) {
			string s = entry.path().u8string();

			if (StringUtils::endsWithIgnoreCase(s, extension))
			{
				files.emplace_back(s);
				counter++;
			}
		}
	}

	return counter;
}
