#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include <regex>
#include <string>
#include <vector>

using namespace std;

class Parser
{
public:

	/**
	* Retrieves all regex matches in string.
	*
	* @param s the string
	* @param rgx the regex
	* @return all matches
	*/
	vector<string>getAllMatchs(string s, regex rgx);
};

#endif // !define PARSER_H_INCLUDED