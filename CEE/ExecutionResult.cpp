#include "ExecutionResult.h"

ExecutionResult::ExecutionResult(ExecutionResultCode code)
{
	this->code = code;
}

bool ExecutionResult::isSuccess()
{
	return code == ExecutionResultCode::Success;
}

int ExecutionResult::getResultCode()
{
	return static_cast<int>(code);
}

string ExecutionResult::getErrorMessage()
{
	if (code == ExecutionResultCode::Success)
		return "";

	auto it = valuedErrors.find(code);

	if (it == valuedErrors.end())
		return "TODO";

	return string(ERR).append(it->second);
}
