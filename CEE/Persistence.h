#ifndef PERSISTENCE_H
#define PERSISTENCE_H

#include "BinaryFile.h"

template <class T>
class Persistence
{
public:

	virtual unsigned int countRegisters() = 0;

	virtual bool writeInto(T obj) = 0;

	virtual bool readAt(T& obj, unsigned int position) = 0;

	virtual	vector<T> readAll() = 0;

	virtual bool removeAt(unsigned int position) = 0;

	virtual bool isGood() = 0;

	template <class Key>
	int find(T& obj, Key key, bool (*compare)(T, Key)) {
		for (unsigned int i = 0; i < countRegisters(); i++) {
			if (readAt(obj, i) && compare(obj, key))
				return i;
		}

		return -1;
	}

	template <class Key, class Key2>
	int find(T& obj, Key key, Key2 anotherKey, bool (*compare)(T, Key, Key2)) {
		for (unsigned int i = 0; i < countRegisters(); i++) {
			if (readAt(obj, i) && compare(obj, key, anotherKey))
				return i;
		}

		return -1;
	}

	template <typename Key>
	vector<T> findAll(Key key, bool(*compare)(T, Key)) {
		T obj;
		vector<T> found;

		for (unsigned int i = 0; i < countRegisters(); i++) {
			if (readAt(obj, i) && compare(obj, key))
				found.emplace_back(obj);
		}

		return found;
	}

	template <typename Key, typename Key2>
	vector<T> findAll(Key key, Key2 anotherKey, bool(*compare)(T, Key, Key2)) {
		T obj;
		vector<T> found;

		for (unsigned int i = 0; i < countRegisters(); i++) {
			if (readAt(obj, i) && compare(obj, key, anotherKey))
				found.emplace_back(obj);
		}

		return found;
	}

	template <typename Key>
	vector<T> findAll2(Key key, Key anotherKey, Key thirdKey, bool(*compare)(T, Key, Key, Key)) {
		T obj;
		vector<T> found;

		for (unsigned int i = 0; i < countRegisters(); i++) {
			if (readAt(obj, i) && compare(obj, key, anotherKey, thirdKey))
				found.emplace_back(obj);
		}

		return found;
	}

protected:
	BinaryFile file;

	inline bool initializeFile(const string fileName)
	{
		file = BinaryFile(fileName);

		return file.is_open();
	}
};

#endif // !PERSISTENCE_H