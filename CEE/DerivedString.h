#ifndef DERIVED_STRING_H
#define DERIVED_STRING_H

constexpr int STRING_LENGHT = 200;

// c string with predefined length.
typedef char DerivedString[STRING_LENGHT];

#endif // !DERIVED_STRING_H

