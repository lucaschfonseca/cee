#include "Parser.h"
#include "Parser.h"

vector<string> Parser::getAllMatchs(string s, regex rgx)
{
	vector<string> matches;
	sregex_iterator it(s.begin(), s.end(), rgx);
	sregex_iterator reg_end;

	for (; it != reg_end; ++it) {
		matches.emplace_back(it->str());
	}

	return matches;
}