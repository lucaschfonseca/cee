#ifndef CLIENT_PERSISTENCE_H
#define CLIENT_PERSISTENCE_H

#include "BinaryFile.h"
#include "Client.h"
#include "Persistence.h"
#include "DerivedString.h"

class ClientPersistence : public Persistence<Client>
{
public:

#pragma region Binary derivation types
	typedef struct BinaryHomeAddress {
		DerivedString city;
		DerivedString zipCode;
		DerivedString state;
		DerivedString street;
		DerivedString neighborhood;
	}BinaryHomeAddress;

	typedef struct BinaryClient {
		DerivedString name;
		DerivedString cpf;
		DerivedString number;

		DerivedString installations[Client::MAXIMUM_INSTALLATIONS_PER_COSTUMER];

		ClientPersistence::BinaryHomeAddress homeAddress;
	}BinaryClient;
#pragma endregion
	
	ClientPersistence();

	~ClientPersistence();

	/**
	* Counts the number of registers in file.
	*
	* @return the number of registers.
	*/
	virtual unsigned int countRegisters() override;

	/**
	* Save a client to file
	*
	* @param obj the client to be saved.
	* @return true if success, false otherwise.
	*/
	virtual bool writeInto(Client obj) override;

	/**
	* Reads a record from a file position
	*
	* @param client The reference of a variable where the record will be stored.
	* @param position the position in file.
	* @return true if success, false otherwise.
	*/
	virtual bool readAt(Client& client, unsigned int position) override;

	/**
	* Reads all records from a file position
	*
	* @return all records.
	*/
	virtual vector<Client> readAll() override;

	/**
	* Removes a record from a file position
	*
	* @param position the position in file.
	* @return true if success, false otherwise.
	*/
	virtual bool removeAt(unsigned int position);

	/**
	* Checks if the file is open and can be used.
	*
	* @return true if is open, false otherwise.
	*/
	virtual bool isGood() override;

	/**
	* Indicates whether cliente number is "equal to" another client number.
	*
	* @param client
	* @param number
	* @return true if equal, false otherwise.
	*/
	static bool compareNumbers(Client client, string number);

private:
	HomeAddress fromStruct(BinaryHomeAddress reg);

	BinaryHomeAddress asStruct(HomeAddress address);

	Client fromStruct(BinaryClient reg);

	BinaryClient asStruct(Client address);
};
#endif // !CLIENT_PERSISTENCE_H