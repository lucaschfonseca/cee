#include <clocale>
#include <iostream>

#include "Cee.h"

#include "ConsumptionFileParser.h"

using namespace std;

int main(int argc, char** argv) {
	setlocale(LC_ALL, "");

	return Cee().execute(argc, argv).getResultCode();
}