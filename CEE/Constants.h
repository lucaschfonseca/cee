#ifndef ELECTRICITY_ENERGY_CONSUMPTION_CONSTANTS_H
#define ELECTRICITY_ENERGY_CONSUMPTION_CONSTANTS_H

constexpr auto COMMAND_SYNTAX_IS_INCORRECT = "A sintaxe do comando est� incorreta.";
constexpr auto CLIENT_NOT_FOUND = "Cliente nao encontrado.";
constexpr auto IMPORTED = "importada";
constexpr auto INVOICE = "fatura";
constexpr auto FACILITIES = "Instala��es";
constexpr auto NAME = "Nome";
constexpr auto NUMBER = "N�mero";
constexpr auto STATE = "Estado";
constexpr auto STREET = "Rua";
constexpr auto ADDRESS = "Endere�o";
constexpr auto CITY = "Cidade";
constexpr auto NEIGHBORHOOD = "Bairro";
constexpr auto ZIP_CODE = "CEP";
constexpr auto ALREADY_IMPORTED = "j� importada(s)";
constexpr auto CLIENT_INVOICE = "Fatura do cliente";
constexpr auto IN_THE_AMMOUTNT_OF = "no valor de";
constexpr auto IMPORT_FAILURES = "falha(s) de importa��o";
constexpr auto CONVERSION_FAILURES = "falha(s) de convers�o";
constexpr auto UNABLE_TO_OPEN_FILE = "Incapaz de abrir o arquivo.";
constexpr auto INVALID_INPUT = "Dados de entrada inv�lidos.";
constexpr auto MONEY_SIGN = "R$";
constexpr auto DAILY_AVERAGE_CONSUMPTION = "Consumo m�dio di�rio";
constexpr auto VALUE_OF_ELECTRIC_CONSUMPTION = "Valor do consumo el�trico: R$ ";
constexpr auto NUMBER_OF_DAYS = "N�mero de dias";
constexpr auto MONTHLY_CONSUMPTION = "Consumo mensal";
constexpr auto CEMIG_INVOICE_DATA = "Dados da fatura Cemig em";
constexpr auto DATA_CALCULATED_FOR = "Dados calculados para";
constexpr auto COMMENT_START_POINT = "#";

//Command parameter
constexpr auto PARAMETER_TXT_IMPORT = "/i";

// Working extensions
constexpr auto EXT_PDF = ".pdf";
constexpr auto EXT_TXT = ".txt";

// Persistence file names
constexpr auto F_INVOICES = "invoices.dat";
constexpr auto F_CLIENT = "clients.dat";

// Temporary directory
constexpr auto TEMP_INVOICE_DIR = "faturas";

// Pdf to text converter command
constexpr auto CONVERT_FROM_PDF_TO_TEXT = "pdftotext -q -table -fixed 5 -marginl 0";
constexpr auto PDF_CONVERT_SUCCESS = 0;
constexpr auto PDF_CONVERT_OPEN_FAILURE = 1;
constexpr auto PDF_CONVERT_OUTPUT_OPEN_FAILURE = 2;
constexpr auto PDF_CONVERT_PERMISSIONS_FAILURE = 3;
constexpr auto PDF_CONVERT_UNKNOWN_ERROR = 99;

constexpr auto FAILED_TO_OPEN_PDF = "Falha na abertura do arquivo.";
constexpr auto FAILED_TO_OPEN_OUTPUT_FILE = "Falha na abertura do arquivo de saida.";
constexpr auto PERMISSION_ERROR = "Erro de permiss�es.";
constexpr auto UNKNOWN_ERROR= "Erro desconhecido.";

// Invoice Keywords
constexpr auto K_ACTUAL_READING = "Leitura Atual";
constexpr auto K_ADDITIONAL_FLAGS = "Adicional Bandeiras - J� inclu�do no Valor a Pagar";
constexpr auto K_AMOUNT = "Quantidade";
constexpr auto K_AMOUNT_PAYABLE = "Valor a pagar";
constexpr auto K_APPLIED_RATES = "Tarifas Aplicadas (sem impostos)";
constexpr auto K_AVERAGE_KWH_DAY = "M�DIA kWh/Dia";
constexpr auto K_BILLED_AMOUNTS = "Valores Faturados";
constexpr auto K_CHARGES = "Encargos/Cobran�as";
constexpr auto K_CLASS = "Classe";
constexpr auto K_CLIENT_NUMBER = "N� DO CLIENTE";
constexpr auto K_CONSUMPTION_KWH = "Consumo kWh";
constexpr auto K_CONSUMPTION_HISTORY = "Hist�rico";
constexpr auto K_CONTRIBUTION_MUNICIPAL_STREET_LIGHTING = "Contrib Ilum Publica Municipal";
constexpr auto K_CPF = "CPF";
constexpr auto K_DAYS = "Dias";
constexpr auto K_DESCRIPTION = "Descri��o";
constexpr auto K_DUE_DATE = "Vencimento";
constexpr auto K_EMISSION_DATE = "Data de Emiss�o";
constexpr auto K_GENERAL_INFORMATION = "Informa��es Gerais";
constexpr auto K_INSTALLATION_NUMBER = "N� DA INSTALA��O";
constexpr auto K_MEASURE = "Medi��o";
constexpr auto K_MEASURE_TYPE = "Tipo de Medi��o";
constexpr auto K_MONTH_YEAR = "M�S/ANO";
constexpr auto K_MULTIPLICATION_CONSTANT = "Constante de Multiplica��o";

constexpr auto K_NEXT_READING_DATE = "PR�XIMA";
constexpr auto K_PREVIOUS_READING_DATE = "ANTERIOR";
constexpr auto K_ACTUAL_READING_DATE = "ATUAL";

constexpr auto K_PREVIOUS_READING = "Leitura Anterior";
constexpr auto K_PRICE = "Pre�o";
constexpr auto K_RATE = "Tarifa";
constexpr auto K_READING_DATES = "Datas de Leitura";
constexpr auto K_RED_FLAG = "Bandeira Vermelha";
constexpr auto K_REGARDING = "Referente a";
constexpr auto K_SUBCLASS = "Subclasse";
constexpr auto K_TARIFF_MODALITY = "Modalidade Tarif�ria";
constexpr auto K_YELLOW_FLAG = "Bandeira Amarela";

// Month Abbreviation
constexpr auto K_JAN = "JAN";
constexpr auto K_FEB = "FEV";
constexpr auto K_MAR = "MAR";
constexpr auto K_ABR = "ABR";
constexpr auto K_MAI = "MAI";
constexpr auto K_JUN = "JUN";
constexpr auto K_JUL = "JUL";
constexpr auto K_AGO = "AGO";
constexpr auto K_SET = "SET";
constexpr auto K_OUT = "OUT";
constexpr auto K_NOV = "NOV";
constexpr auto K_DEZ = "DEZ";

// Error messages
constexpr auto ERR = "ERR: ";
constexpr auto DIRECTORY_NOT_FOUND = "Diret�rio n�o encontrado.";
constexpr auto IMPORTATION_ERROR = "Importa��o finalizada com erros.";
constexpr auto INFO = "INFO: ";
constexpr auto INCORRECT_SYNTAX = "Sintaxe incorreta.";
constexpr auto NO_INVOICES_FOUND = "Nenhuma fatura encontrada.";
constexpr auto UNABLE_TO_CREATE_PERSISTENCE_FILES = "Incapaz de criar os arquivos de persist�ncia.";
constexpr auto UNABLE_TO_CREATE_TEMPORARY_DIRECTORY = "Incapaz de criar diret�rio tempor�rio.";

#endif //ELECTRICITY_ENERGY_CONSUMPTION_CONSTANTS_H