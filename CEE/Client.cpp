#include "Client.h"
#include "StringUtils.h"

#pragma region Getters & Setters

Client& Client::setName(string name) {
	this->name = name;
	return *this;
}

string Client::getName() {
	return name;
}

string Client::getCapitalizedName()
{
	return StringUtils::capitalizeWords(getName());
}

Client& Client::setCpf(string cpf) {
	this->cpf = cpf;
	return *this;
}

string Client::getCpf() {
	return cpf;
}

Client& Client::setNumber(string number) {
	this->number = number;
	return *this;
}

string Client::getNumber() {
	return number;
}

vector<string> Client::getInstallations() {
	return installations;
}

Client& Client::setHomeAddress(HomeAddress adress)
{
	this->homeAddress = adress;
	return *this;
}

HomeAddress& Client::getHomeAddress()
{
	return homeAddress;
}

#pragma endregion

bool Client::addInstallation(string installationNumber) {
	if (installations.size() < MAXIMUM_INSTALLATIONS_PER_COSTUMER) {
		this->installations.emplace_back(installationNumber);
		return true;
	}

	return false;
}

string Client::toString()
{
	string str = string().append(NAME).append(": ").append(name).append("\n")
		.append(K_CLIENT_NUMBER).append(": ").append(number).append("\n")
		.append(K_CPF).append(": ").append(cpf).append("\n")
		.append(homeAddress.toString()).append(FACILITIES).append(": ")
		;

	for (string s : installations) {
		if (!StringUtils::endsWith(str, ": "))
			str.append(", ");

		str.append(s);
	}

	return str;
}

bool Client::containsInstallation(string facilityNumber)
{
	for (string s : installations) {
		if (s == facilityNumber)
			return true;
	}

	return false;
}