#include <iostream>
#include "BinaryFile.h"

BinaryFile::BinaryFile()
{
}

BinaryFile::BinaryFile(const string& fileName) {
	
	open(fileName, ios::app | ios::out | ios::in | ios::binary);
}

void BinaryFile::openFile(const string & fileName)
{
	open(fileName, ios::app | ios::out | ios::in | ios::binary);
}

void BinaryFile::close()
{
	fstream::close();
}

bool BinaryFile::is_open()
{
	return fstream::is_open();
}

void BinaryFile::open(const string& _Str, ios_base::openmode _Mode, int _Prot)
{
	this->name = _Str;
	fstream::open(_Str, ios::app | ios::out | ios::in | ios::binary);
}

unsigned long BinaryFile::getSize() {
	seekp(0, ios::beg);
	unsigned long begin = static_cast<unsigned long>(tellp());

	seekp(0, ios::end);
	unsigned long end = static_cast<unsigned long>(tellp());

	return end - begin;
}

string BinaryFile::getName() {
	return name;
}