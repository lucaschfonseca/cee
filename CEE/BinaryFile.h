#ifndef ELECTRICITY_ENERGY_CONSUMPTION_BINARY_FILE_H
#define ELECTRICITY_ENERGY_CONSUMPTION_BINARY_FILE_H

#include <string>
#include <fstream>
#include <vector>

using namespace std;

class BinaryFile : private fstream {

public:

	BinaryFile();

	/**
	 * Instantiate a binaryFile and try to open it with a given name.
	 * @param fileName
	 */
	BinaryFile(const string& fileName);

	void openFile(const string& fileName);

	void close();

	bool is_open();

	void open(const string& _Str, ios_base::openmode _Mode = ios_base::in | ios_base::out,
		int _Prot = (int)ios_base::_Openprot);

	/**
	 * @return the size of the file
	 */
	unsigned long getSize();

	/**
	 * @return the name of file
	 */
	string getName();

	/**
	 * Counts the number of records in the file.
	 *
	 * @tparam T the record type.
	 * @return the number of records in the file
	 */
	template<typename T>
	unsigned int countRegisters() {
		return  getSize() / sizeof(T);
	}

	/**
	 * Writes a record to the file.
	 *
	 * @tparam T the record type.
	 * @param obj the record to be written.
	 * @return true if successful false if failed.
	 */
	template<typename T>
	bool writeInto(T obj) {
		seekp(0, ios::end);
		return this->write(reinterpret_cast<char*>(std::addressof(obj)), sizeof(T)) ? true : false;
	}

	/**
	 * Reads a record of the file and store it in the variable referenced by obj.
	 *
	 * @tparam T the record type.
	 * @param obj reference of a type T variable where the read register will be storing.
	 * @return true if successful false if failed.
	 */
	template <typename T>
	bool readFrom(T& obj, unsigned int position) {
		unsigned int count = countRegisters<T>();

		if (count >= 0 && position < count) {
			seekg(position * sizeof(T), ios::beg);

			return this->read(reinterpret_cast<char*>(std::addressof(obj)), sizeof(T)) ? true : false;
		}

		return false;
	}

	/**
	 * Reads all records from the file and store in the variable referenced by all.
	 *
	 * @tparam T the record type.
	 * @param all reference of a vector where the registers will be stored.
	 */
	template <typename T>
	vector<T> readAll() {
		T obj;
		vector<T> all;

		unsigned int count = countRegisters<T>();

		if (count > 0) {
			seekg(0, ios::beg);

			for (unsigned int i = 0; i < count && good(); i++)
			{
				if (this->read(reinterpret_cast<char*>(std::addressof(obj)), sizeof(T)))
					all.emplace_back(obj);
			}
		}
		return all;
	}

	/**
	 * Searches a record in the file.
	 *
	 * @tparam T the record type.
	 * @tparam Key the search key type.
	 * @param obj reference of a variable where will be stored the record, if found.
	 * @param key the search key.
	 * @param comparator pointer to the compare function.
	 * @return the position in file if found, -1 otherwise.
	 */
	template <class T, typename Key>
	int find(T& obj, Key key, bool(*comparator)(T, Key)) {
		if (getSize() > 0) {
			this->seekg(0, ios::beg);
			int counter = 0;

			while (good())
			{
				this->read(reinterpret_cast<char*>(std::addressof(obj)), sizeof(T));
				if (comparator(obj, key))
					return counter;

				counter++;
			}
		}

		return -1;
	}

	/**
	 * Search all occurrences of a given key in the file.
	 *
	 * @tparam T the record type.
	 * @tparam Key the search key type.
	 * @param key the search key.
	 * @param comparator pointer to the compare function.
	 * @param all reference of a vector where the registers will be stored.
	 * @return the number of occurrences.
	 */
	template <class T, typename Key>
	int findAll(vector<T>& all, Key key, bool(*comparator)(T, Key)) {
		int counter = 0;

		if (countRegisters<T>() > 0) {
			T obj;

			all.clear();
			seekg(0, ios::beg);

			while (good())
			{
				this->read(reinterpret_cast<char*>(std::addressof(obj)), sizeof(T));

				if (comparator(obj, key)) {
					all.emplace_back(obj);
					counter++;
				}
			}
		}
		return counter;
	}

	/*
	 * Deletes a record from the file considering zero (0) as the first position.
	 *
	 * @tparam T the record type.
	 * @param position The position of the record in the file.
	 * @return true if removed, false otherwise.
	*/
	template <typename T>
	bool removeAt(unsigned int position) {
		unsigned int count = countRegisters<T>();

		if (count >= 0 && position < count) {
			vector<T> all = readAll<T>();

			close();
			remove(getName().c_str());
			open(getName());

			// Copy all records from file to temporary file.
			for (unsigned int i = 0; i < all.size(); i++) {
				if (i != position)
					writeInto(all.at(i));
			}

			return true;
		}

		return false;
	}

private:
	string name;
};

#endif //ELECTRICITY_ENERGY_CONSUMPTION_BINARY_FILE_H


