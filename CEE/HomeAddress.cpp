#include "HomeAddress.h"
#include "Constants.h"
#include "StringUtils.h"

#pragma region Getters & Setters
HomeAddress& HomeAddress::setCity(string city)
{
	this->city = city;
	return *this;
}

string HomeAddress::getCity()
{
	return city;
}

HomeAddress& HomeAddress::setZipCode(string zipCode)
{
	this->zipCode = zipCode;
	return *this;
}

string HomeAddress::getZipCode()
{
	return zipCode;
}

HomeAddress& HomeAddress::setNeighborhood(string neighborhood)
{
	this->neighborhood = neighborhood;
	return *this;
}

string HomeAddress::getNeighborhood()
{
	return neighborhood;
}

HomeAddress& HomeAddress::setState(string state)
{
	this->state = state;
	return *this;
}

string HomeAddress::getState()
{
	return state;
}

HomeAddress& HomeAddress::setStreet(string street)
{
	this->street = street;
	return *this;
}

string HomeAddress::getStreet()
{
	return street;
}

#pragma endregion

string HomeAddress::toString()
{
	return string().append(STATE).append(": ").append(state).append("\n")
		.append(CITY).append(": ").append(city).append("\n")
		.append(ZIP_CODE).append(": ").append(zipCode).append("\n")
		.append(NEIGHBORHOOD).append(": ").append(neighborhood).append("\n")
		.append(STREET).append(": ").append(street).append("\n")
		;
}