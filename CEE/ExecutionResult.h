#ifndef EXECUTION_RESULT_H
#define EXECUTION_RESULT_H

#include <string>
#include <map>

#include "Constants.h"

using namespace std;

enum class ExecutionResultCode
{
	Success,
	ClientNotFound,
	UnableToOpenPersistenceFiles,
	UnableToOpenFile,
	UnableToCreateTemporaryDirectory,
	IncorrectSyntax,
	InvalidInput,
	DirectoryNotFound,
	NoInvoicesFound,
	ImportationError
};


/**
* Represents the return of the execution of a function. With this class you can get the error code as well as the message associated with it
*/
class ExecutionResult
{
public:
	ExecutionResult(ExecutionResultCode code);

	/**
	* Checks if the execution was successful.
	*
	* @return true if success, false otherwise.
	*/
	bool isSuccess();

	/**
	* Retrieves a code for the result.
	*
	* @return the code.
	*/
	int getResultCode();

	/**
	* The error message.
	*
	* @return the error description if has error, empty if success.
	*/
	string getErrorMessage();

private:
	map<ExecutionResultCode, string> valuedErrors = {
		{ExecutionResultCode::UnableToOpenPersistenceFiles, UNABLE_TO_CREATE_PERSISTENCE_FILES},
		{ExecutionResultCode::UnableToCreateTemporaryDirectory, UNABLE_TO_CREATE_TEMPORARY_DIRECTORY},
		{ExecutionResultCode::IncorrectSyntax, INCORRECT_SYNTAX},
		{ExecutionResultCode::DirectoryNotFound, DIRECTORY_NOT_FOUND},
		{ExecutionResultCode::ImportationError, IMPORTATION_ERROR},
		{ExecutionResultCode::NoInvoicesFound, NO_INVOICES_FOUND},
		{ExecutionResultCode::ClientNotFound, CLIENT_NOT_FOUND},
		{ExecutionResultCode::UnableToOpenFile, UNABLE_TO_OPEN_FILE},
		{ExecutionResultCode::InvalidInput, INVALID_INPUT},
	};

	string message;

	ExecutionResultCode code;
};

#endif // !EXECUTION_RESULT_H

