#ifndef DIRECTORY_UTILS_H
#define DIRECTORY_UTILS_H

#include <string>
#include <vector>

/**
* disables deprecated the error for  <experimental/filesystem> 
*
* Severity	Code	Description	Project	File	Line	Suppression State	Suppression State
* Error (active)	E0035	#error directive: The <experimental/filesystem> header providing std::experimental::filesystem
* is deprecated by Microsoft and will be REMOVED. It is superseded by the C++17 <filesystem> header providing std::filesystem.
* You can define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING to acknowledge that you have received this warning.
* CEE	C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Tools\MSVC\14.23.28105\include\experimental\filesystem	24*
*
* NOTE: Estou usando esse artificio pois o projeto tem como requisito executar nos computadores do IFET que atualmente tem o compilador C++11
*
* Se este nao fosse o caso, nao seria usada a bliblioteca <experimental/filesystem> e sim <filesystem>.
*/
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING 

#include <experimental/filesystem>

using namespace std;

class DirectoryUtils
{
public:

	/**
	 * Check if there is a directory with a certain path.
	 *
	 * @param path the path to be checked.
	 * @return true if exists, false otherwise.
	 */
	static bool exists(const string& path);

	/**
	* Creates a directory for a given path.
	*
	* @param path the absolute path to the directory.
	* @return true if created, false otherwise.
	*/
	static bool create(const string& path);

	/**
	* Checks if a directory exists and tries to create if it does not exist
	*
	* @param path the absolute path of the directory.
	* @return true if exists or created, false otherwise.
	*/
	static bool createIfNotExists(const string& path);

	/**
	* Retrieve a listing of all directory files
	*
	* @param path the absolute path of the directory.
	* @param files The reference of a variable where the record will be stored.
	* @param extension the extension to be listed.
	* @return the number files of the directory.
	*/
	static int getAllFiles(const string& path, vector<string>& files, const string& extension);
};

#endif // !DIRECTORY_UTILS_H
