#include "ConsumptionItem.h"

string ConsumptionItem::getDescription()
{
	return description;
}

ConsumptionItem& ConsumptionItem::setDescription(string description)
{
	this->description = description;
	return *this;
}

double ConsumptionItem::getWatts()
{
	return watts;
}

ConsumptionItem& ConsumptionItem::setWatts(double watts)
{
	this->watts = watts;
	return *this;
}

double ConsumptionItem::getHours()
{
	return hours;
}

ConsumptionItem& ConsumptionItem::setHours(double hours)
{
	this->hours = hours;
	return *this;
}

double ConsumptionItem::getKwh()
{
	if (kWh == 0)
		kWh = (getWatts() / 1000) * getHours();

	return kWh;
}

ConsumptionItem& ConsumptionItem::setKwh(double kwh)
{
	this->kWh = kwh;
	return *this;
}

string ConsumptionItem::toString()
{
	string str = string(getDescription()).append(" ");

	if (kWh != 0)
		str.append(to_string(kWh)).append(" kWh");
	else
		str.append(to_string(watts)).append(" W ")
		.append(to_string(hours)).append(" h");

	return str;
}
