#include "InvoicePersistence.h"
#include "StringUtils.h"
#include <regex>

InvoicePersistence::InvoicePersistence()
{
	initializeFile(F_INVOICES);
}

InvoicePersistence::~InvoicePersistence()
{
	if (file.is_open())
		file.close();
}

unsigned int InvoicePersistence::countRegisters()
{
	unsigned size = file.getSize();

	return size ? (size / sizeof(BinaryInvoice)) : 0;
}

bool InvoicePersistence::writeInto(Invoice obj)
{
	return file.writeInto(asStruct(obj)) ? true : false;
}

bool InvoicePersistence::readAt(Invoice& invoice, unsigned int position)
{
	BinaryInvoice obj;
	Client client;

	bool read = file.readFrom(obj, position);

	if (read) {
		fClient.find<const string>(client, string(obj.clientNumber), fClient.compareNumbers);

		invoice = (fromStruct(obj));
		invoice.setClient(client);
	}
	return  read;
}

vector<Invoice> InvoicePersistence::readAll()
{
	vector<Invoice> all;
	Client client;

	for (BinaryInvoice reg : file.readAll<BinaryInvoice>()) {
		Invoice invoice = fromStruct(reg);
		fClient.find<const string>(client, string(reg.clientNumber), fClient.compareNumbers);
		invoice.setClient(client);

		all.emplace_back(invoice);
	}

	return all;
}

bool InvoicePersistence::removeAt(unsigned int position)
{
	return file.removeAt<BinaryInvoice>(position);
}

bool InvoicePersistence::isGood()
{
	return fClient.isGood() && file.is_open();
}

#pragma region Class to struct | struct to class parser
InvoicePersistence::BinaryConsumptionHistory InvoicePersistence::asStruct(MonthlyConsumption consumption)
{
	BinaryConsumptionHistory derived;

	derived.averageKWhPerDay = consumption.getAverageKWhPerDay();

	derived.daysCount = consumption.getDaysCount();
	derived.year = consumption.getYear();
	derived.kwh = consumption.getKwh();

	StringUtils::toDerivedString(consumption.getMonth(), derived.month);

	return derived;
}

Invoice InvoicePersistence::fromStruct(BinaryInvoice reg)
{
	Invoice invoice;

	invoice.setTotalValue(reg.totalValue);
	invoice.setMunicipalLightingContribution(reg.municipalLightingContribution);
	invoice.setElectricityPrice(reg.electricityPrice);
	invoice.setRedFlag(reg.redFlag);
	invoice.setYellowFlag(reg.yellowFlag);
	invoice.setPreviousRead(reg.previousRead);
	invoice.setActualRead(reg.actualRead);

	invoice.setKwhConsumption(reg.kwhConsumption);

	invoice.setInstallationNumber(string(reg.installationNumber));
	invoice.setRegarding(string(reg.regarding));
	invoice.setDueDate(string(reg.dueDate));
	invoice.setActualReadingDate(string(reg.actualReadingDate));
	invoice.setPreviousReadingDate(string(reg.previousReadingDate));
	invoice.setNextReadingDate(string(reg.nextReadingDate));

	for (int i = 0; i < Invoice::HISTORY_MONTHS_COUNT; i++) {
		invoice.addConsumptionEntry(fromStruct(reg.consumptionHistory[i]));
	}

	return invoice;
}

InvoicePersistence::BinaryInvoice InvoicePersistence::asStruct(Invoice invoice)
{
	BinaryInvoice derived;

	derived.totalValue = invoice.getTotalValue();
	derived.municipalLightingContribution = invoice.getMunicipalLightingContribution();
	derived.electricityPrice = invoice.getElectricityPrice();
	derived.redFlag = invoice.getRedFlag();
	derived.yellowFlag = invoice.getYellowFlag();
	derived.previousRead = invoice.getPreviousRead();
	derived.actualRead = invoice.getActualRead();

	derived.kwhConsumption = invoice.getKwhConsumption();

	StringUtils::toDerivedString(invoice.getClient().getNumber(), derived.clientNumber);
	StringUtils::toDerivedString(invoice.getInstallationNumber(), derived.installationNumber);
	StringUtils::toDerivedString(invoice.getRegarding(), derived.regarding);
	StringUtils::toDerivedString(invoice.getDueDate(), derived.dueDate);
	StringUtils::toDerivedString(invoice.getActualReadingDate(), derived.actualReadingDate);
	StringUtils::toDerivedString(invoice.getPreviousReadingDate(), derived.previousReadingDate);
	StringUtils::toDerivedString(invoice.getNextReadingDate(), derived.nextReadingDate);

	for (unsigned int i = 0; i < Invoice::HISTORY_MONTHS_COUNT; i++)
		derived.consumptionHistory[i] = asStruct(invoice.getConsumptionHistory().at(i));

	return derived;
}

MonthlyConsumption InvoicePersistence::fromStruct(BinaryConsumptionHistory reg)
{
	MonthlyConsumption destination;

	destination.setAverageKWhPerDay(reg.averageKWhPerDay);

	destination.setDaysCount(reg.daysCount);
	destination.setYear(reg.year);
	destination.setKwh(reg.kwh);

	destination.setMonth(string(reg.month));

	return destination;
}
#pragma endregion

bool InvoicePersistence::compareInstallationNumbers(Invoice invoice, string number)
{
	return invoice.getInstallationNumber() == number;
}

bool InvoicePersistence::compareClientNumbers(Invoice invoice, string clientNumber)
{
	return invoice.getClient().getNumber() == clientNumber;
}

bool InvoicePersistence::compareRegarding(Invoice invoice, string regarding)
{
	return StringUtils::equalsIgnoreCase(invoice.getRegarding(), regarding);
}

bool InvoicePersistence::compareClientAndRegarding(Invoice invoice, string clientNumber, string regarding)
{
	return  compareClientNumbers(invoice, clientNumber) && compareRegarding(invoice, regarding);
}

bool InvoicePersistence::compareInstallationRegarding(Invoice invoice, string installation, string regarding)
{
	return compareInstallationNumbers(invoice, installation) && compareRegarding(invoice, regarding);
}

bool InvoicePersistence::compareClientAndPeriod(Invoice invoice, const string& clientNumber, const string& periodBegin,
	const string& periodEnd)
{
	if (!StringUtils::contains(periodBegin, "/") || !StringUtils::contains(periodEnd, "/"))
		return false;

	string beginMonth = periodBegin.substr(0, periodBegin.find("/"));
	string endMonth = periodEnd.substr(0, periodEnd.find("/"));

	int beginYear = StringUtils::toInt(periodBegin.substr(periodBegin.find_first_not_of(beginMonth) + 1));
	int endYear = StringUtils::toInt(periodEnd.substr(periodEnd.find_first_not_of(endMonth) + 1));

	while (beginYear > 99)
		beginYear = beginYear % 100;

	while (endYear > 99)
		endYear = endYear % 100;

	if (beginYear == 0 || endYear == 0)
		return false;

	if (!compareClientNumbers(invoice, clientNumber))
		return false;

	return invoice.containsHistoryFromMonthAndYear(beginYear, beginMonth)
		|| invoice.containsHistoryFromMonthAndYear(endYear, endMonth);
}