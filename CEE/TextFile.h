#ifndef ELECTRICITY_ENERGY_CONSUMPTION_TEXT_FILE_H
#define ELECTRICITY_ENERGY_CONSUMPTION_TEXT_FILE_H

#include <string>
#include <vector>
#include <fstream>

using namespace std;

enum class FileOpenMode {
	APPEND,
	WRITE,
	READ
};

class TextFile : public fstream {
public:

	/**
	 * instantiates a Text file and tries to open it.
	 *
	 * @param fileName the name of file to be open.
	 * @param mode the open mode
	 */
	TextFile(string fileName, enum class FileOpenMode mode);

	/**
	 * Retrieves the contents of the file separate line by line.
	 *
	 * @return a vector containing the lines.
	 */
	vector<string> getLines();

	/**
	 * Retrieves the contents of the file in a single line.
	 *
	 * @return the content.
	 */
	string getContent();

	/**
	 * @return the size of the file
	 */
	unsigned int getSize();

	/**
	 * @return the name used to open the file
	 */
	string getName();

private:
	string name;
};


#endif //ELECTRICITY_ENERGY_CONSUMPTION_TEXT_FILE_H
