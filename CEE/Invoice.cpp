#include "Invoice.h"
#include "StringUtils.h"
#include <iostream>
#include <map>

#pragma region Getters & Setters
Invoice& Invoice::setActualRead(double value)
{
	this->actualRead = value;
	return *this;
}

double Invoice::getActualRead()
{
	return actualRead;
}

Invoice& Invoice::setKwhConsumption(double consumption)
{
	this->kwhConsumption = consumption;
	return *this;
}

double Invoice::getKwhConsumption()
{
	return kwhConsumption;
}

Invoice& Invoice::setInstallationNumber(string number)
{
	this->installationNumber = number;
	client.addInstallation(number);
	return *this;
}

string Invoice::getInstallationNumber()
{
	return installationNumber;
}

double Invoice::getPreviousRead()
{
	return previousRead;
}

Invoice& Invoice::setPreviousRead(double value)
{
	this->previousRead = value;
	return *this;
}

Invoice& Invoice::setRegarding(string regarding)
{
	if (!regarding.empty()) {
		this->regarding = regarding;
	}

	return *this;
}

string Invoice::getRegarding()
{
	return regarding;
}

Invoice& Invoice::setDueDate(string dueDate)
{
	this->dueDate = dueDate;
	return *this;
}

string Invoice::getDueDate()
{
	return dueDate;
}

Invoice& Invoice::setActualReadingDate(string date)
{
	this->actualReadingDate = date;
	return *this;
}

string Invoice::getActualReadingDate()
{
	return actualReadingDate;
}

Invoice& Invoice::setPreviousReadingDate(string date)
{
	this->previousReadingDate = date;
	return *this;
}

string Invoice::getPreviousReadingDate()
{
	return previousReadingDate;
}

Invoice& Invoice::setNextReadingDate(string date)
{
	this->nextReadingDate = date;
	return *this;
}

string Invoice::getNextReadingDate()
{
	return nextReadingDate;
}

const vector<MonthlyConsumption>& Invoice::getConsumptionHistory()
{
	return consumptionHistory;
}

Invoice& Invoice::setClient(Client client)
{
	this->client = client;
	return *this;
}

Client& Invoice::getClient()
{
	return this->client;
}

Invoice& Invoice::setTotalValue(double totalValue)
{
	this->totalValue = totalValue;
	return *this;
}

double Invoice::getTotalValue()
{
	return totalValue;
}

Invoice& Invoice::setMunicipalLightingContribution(double contribution)
{
	this->municipalLightingContribution = contribution;
	return *this;
}

Invoice& Invoice::setMunicipalLightingContribution(string contribution)
{
	this->municipalLightingContribution = StringUtils::toDouble(contribution);
	return *this;
}

double Invoice::getMunicipalLightingContribution()
{
	return municipalLightingContribution;
}

Invoice& Invoice::setElectricityPrice(double price)
{
	this->electricityPrice = price;
	return *this;
}

double Invoice::getElectricityPrice()
{
	return electricityPrice;
}

Invoice& Invoice::setYellowFlag(double yellowFlag)
{
	this->yellowFlag = yellowFlag;
	return *this;
}

double Invoice::getYellowFlag()
{
	return yellowFlag;
}

Invoice& Invoice::setRedFlag(double redFlag)
{
	this->redFlag = redFlag;
	return *this;
}

double Invoice::getRedFlag()
{
	return redFlag;
}
#pragma endregion

void Invoice::createComsumptionHistory()
{
	map<int, string> months;
	vector<MonthlyConsumption> history;

	int keyReferenceMonth = 0;
	string referenceMonth = regarding.substr(0, 3);
	int referenceYear = StringUtils::toInt(regarding.substr(referenceMonth.length() + 1));

#pragma region Month InvoiceTokens initialization
	months[1] = K_JAN;
	months[2] = K_FEB;
	months[3] = K_MAR;
	months[4] = K_ABR;
	months[5] = K_MAI;
	months[6] = K_JUN;
	months[7] = K_JUL;
	months[8] = K_AGO;
	months[9] = K_SET;
	months[10] = K_OUT;
	months[11] = K_NOV;
	months[12] = K_DEZ;
#pragma endregion

	std::map<int, string>::iterator it;

	for (it = months.begin(); it != months.end(); ++it) {
		if (it->second == referenceMonth) {
			keyReferenceMonth = it->first;
			break;
		}
	}

	while (it != months.begin()) {
		history.emplace(history.end(), MonthlyConsumption(it->second, referenceYear));
		--it;
	}

	history.emplace(history.end(), MonthlyConsumption(it->second, referenceYear));

	for (auto rit = months.rbegin(); rit->first >= keyReferenceMonth; ++rit) {
		history.emplace(history.end(), MonthlyConsumption(rit->second, referenceYear - 1));
	}

	this->consumptionHistory = history;
}

bool Invoice::isConsumptionEntryComplete()
{
	return consumptionHistory.size() == HISTORY_MONTHS_COUNT;
}

bool Invoice::addConsumptionEntry(MonthlyConsumption consumption)
{
	if (consumptionHistory.size() == HISTORY_MONTHS_COUNT)
		return false;

	consumptionHistory.emplace_back(consumption);
	return true;
}

bool Invoice::containsHistoryFromMonthAndYear(int year, string month)
{
	for (MonthlyConsumption c : consumptionHistory) {
		if (year == c.getYear() && month == c.getMonth())
			return true;
	}

	return false;
}

MonthlyConsumption Invoice::getCurrentMontlyConsumption()
{
	if (!StringUtils::contains(regarding, "/"))
		return MonthlyConsumption();

	string month = regarding.substr(0, regarding.find("/"));
	int year = StringUtils::toInt(regarding.substr(regarding.find_first_not_of(month) + 1));

	for (MonthlyConsumption c : consumptionHistory)
		if (c.getYear() + 2000 == year  && c.getMonth() == month)
			return c;

	return MonthlyConsumption();
}

string Invoice::toString()
{
	string s = string().append(client.toString()).append("\n")
		.append(K_INSTALLATION_NUMBER).append(": ").append(installationNumber).append("\n")
		.append(K_AMOUNT_PAYABLE).append(": ").append(to_string(totalValue)).append("\n")
		.append(K_CONTRIBUTION_MUNICIPAL_STREET_LIGHTING).append(": ")
		.append(to_string(municipalLightingContribution)).append("\n")
		.append(K_PRICE).append(": ").append(to_string(electricityPrice)).append("\n")
		.append(K_CONSUMPTION_KWH).append(": ").append(to_string(kwhConsumption)).append("\n")
		.append(K_REGARDING).append(": ").append(regarding).append("\n")
		.append(K_DUE_DATE).append(": ").append(dueDate).append("\n")
		.append(K_ACTUAL_READING_DATE).append(": ").append(actualReadingDate).append("\n")
		.append(K_NEXT_READING_DATE).append(": ").append(nextReadingDate).append("\n")
		.append(K_PREVIOUS_READING_DATE).append(": ").append(previousReadingDate).append("\n")
		.append(K_PREVIOUS_READING).append(": ").append(to_string(previousRead)).append("\n")
		.append(K_ACTUAL_READING).append(": ").append(to_string(actualRead)).append("\n")
		.append(K_YELLOW_FLAG).append(": ").append(to_string(yellowFlag)).append("\n")
		.append(K_RED_FLAG).append(": ").append(to_string(redFlag)).append("\n\n")
		.append(K_CONSUMPTION_HISTORY).append(": ").append("\n\n").append(K_MONTH_YEAR).append("\t").
		append(K_CONSUMPTION_KWH).append("\t").append(K_AVERAGE_KWH_DAY).append("\t").append(K_DAYS).append("\n")
		;

	for (auto c : consumptionHistory) {
		s.append(c.toString());
	}

	return s;
}