#include "MonthlyConsumption.h"
#include "StringUtils.h"
#include "Constants.h"

MonthlyConsumption::MonthlyConsumption() {
	this->month = "";
	this->year = 0;
	this->averageKWhPerDay = 0;
	this->kwh = 0;
	this->daysCount = 0;
}

MonthlyConsumption::MonthlyConsumption(string month, int year)
{
	this->month = month;
	this->year = year;
	this->averageKWhPerDay = 0;
	this->kwh = 0;
	this->daysCount = 0;
}

#pragma region Getters & Setters
string MonthlyConsumption::getMonth()
{
	return month;
}

MonthlyConsumption& MonthlyConsumption::setMonth(string month)
{
	this->month = month;
	return *this;
}

int MonthlyConsumption::getYear()
{
	return year;
}

MonthlyConsumption& MonthlyConsumption::setYear(int year)
{
	this->year = year;
	return *this;
}

double MonthlyConsumption::getKwh()
{
	return kwh;
}

MonthlyConsumption& MonthlyConsumption::setKwh(double kwh)
{
	this->kwh = kwh;
	return *this;
}

int MonthlyConsumption::getDaysCount()
{
	return daysCount;
}

MonthlyConsumption& MonthlyConsumption::setDaysCount(int days)
{
	this->daysCount = days;
	return *this;
}

double MonthlyConsumption::getAverageKWhPerDay()
{
	return averageKWhPerDay;
}

MonthlyConsumption& MonthlyConsumption::setAverageKWhPerDay(double averageKWhPerDay)
{
	this->averageKWhPerDay = averageKWhPerDay;
	return*this;
}

string MonthlyConsumption::getJoinedMonthYear()
{
	return string(month).append("/").append(to_string(year));
}
#pragma endregion

string MonthlyConsumption::toString()
{
	return string()
		.append(MONTHLY_CONSUMPTION).append(": ").append(StringUtils::toStringWithPrecision(getKwh())).append(" kwh\n")
		.append(DAILY_AVERAGE_CONSUMPTION).append(": ").append(StringUtils::toStringWithPrecision(getAverageKWhPerDay())).append(" kwh\n")
		.append(NUMBER_OF_DAYS).append(": ").append(StringUtils::toStringWithPrecision(getDaysCount(), 0)).append("\n")
		;
}

int MonthlyConsumption::getIntegerMonth()
{
	auto it = months.find(getMonth());

	if (it != months.end())
		return it->second;

	return 0;
}

void MonthlyConsumption::formatYearAsTwoPlaces()
{
	while (year > 99)
		year = year % 100;
}

bool MonthlyConsumption::isBefore(MonthlyConsumption another)
{
	if (another.getYear() < getYear())
		return false;

	if (another.getYear() == getYear())
		return getIntegerMonth() < another.getIntegerMonth();

	return true;
}

bool MonthlyConsumption::isAfter(MonthlyConsumption another)
{
	if (another.getYear() > getYear())
		return false;

	if (another.getYear() == getYear())
		return getIntegerMonth() > another.getIntegerMonth();

	return true;
}

bool MonthlyConsumption::refersSameMonth(MonthlyConsumption another)
{
	if (another.getYear() != getYear())
		return false;

	return getIntegerMonth() == another.getIntegerMonth();
}
