#ifndef INVOICE_PERSISTENCE_H
#define INVOICE_PERSISTENCE_H

#include <vector>

#include "BinaryFile.h"
#include "ClientPersistence.h"
#include "Invoice.h"

class InvoicePersistence : public Persistence<Invoice>
{
public:

#pragma region Binary derivation types
	typedef struct BinaryConsumptionHistory {
		int year;
		double kwh;
		int daysCount;

		double averageKWhPerDay;

		DerivedString month;
	}BinaryConsumptionHistory;

	typedef struct BinaryInvoice {
		double totalValue;
		double municipalLightingContribution;
		double electricityPrice;
		double redFlag;
		double yellowFlag;
		double previousRead;
		double actualRead;
		double kwhConsumption;

		DerivedString installationNumber;
		DerivedString clientNumber;
		DerivedString regarding;
		DerivedString dueDate;
		DerivedString actualReadingDate;
		DerivedString previousReadingDate;
		DerivedString nextReadingDate;

		BinaryConsumptionHistory consumptionHistory[Invoice::HISTORY_MONTHS_COUNT];
	}BinaryInvoice;
#pragma endregion

	InvoicePersistence();

	~InvoicePersistence();


	virtual unsigned int countRegisters() override;

	virtual bool writeInto(Invoice obj) override;

	virtual bool readAt(Invoice& invoice, unsigned int position) override;

	virtual vector<Invoice> readAll() override;

	virtual bool removeAt(unsigned int position) override;

	virtual bool isGood() override;

	/**
	* Indicates whether invoice installation number is "equal to" an installation number.
	*
	* @param invoice
	* @param installationNumber
	* @return true if equal, false otherwise.
	*/
	static bool compareInstallationNumbers(Invoice invoice, string installationNumber);

	/**
	* Indicates whether invoice client number is "equal to" a client number.
	*
	* @param invoice
	* @param clientNumer
	* @return true if equal, false otherwise.
	*/
	static bool compareClientNumbers(Invoice invoice, string clientNumber);

	/**
	* Indicates whether invoice regarding is "equal to" a regarding.
	*
	* @param invoice
	* @param regarding
	* @return true if equal, false otherwise.
	*/
	static bool compareRegarding(Invoice invoice, string regarding);

	/**
	* Indicates whether invoice regarding is "equal to" a regarding and invoice client number is "equal to" a client number.
	*
	* @param invoice
	* @param regarding
	* @param clientNumer
	* @return true if equal, false otherwise.
	*/
	static bool compareClientAndRegarding(Invoice invoice, string clientNumber, string regarding);

	/**
	* Indicates whether invoice regarding is "equal to" a regarding and invoice installation number is "equal to" an installation number.
	*
	* @param invoice
	* @param regarding
	* @param installation
	* @return true if equal, false otherwise.
	*/
	static bool compareInstallationRegarding(Invoice invoice, string installation, string regarding);

	/**
	* Indicates whether invoice clientNumber is "equal to" a clientNumber and if the invoice has history between a period
	*
	* @param invoice
	* @param clientNumber
	* @param periodBegin
	* @param periodBegin
	* @return true or false.
	*/
	static bool compareClientAndPeriod(Invoice invoice, const string &clientNumber, const string& periodBegin,
		const string& periodEnd);

private:
	ClientPersistence fClient;

	BinaryConsumptionHistory asStruct(MonthlyConsumption consumption);

	MonthlyConsumption fromStruct(BinaryConsumptionHistory derived);

	BinaryInvoice asStruct(Invoice invoice);

	Invoice fromStruct(BinaryInvoice derived);
};

#endif // !INVOICE_PERSISTENCE_H