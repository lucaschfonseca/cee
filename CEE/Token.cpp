#include "Token.h"

bool Token::isOptional()
{
	return optional;
}

Token::Token(string key, string comparator, unsigned int jumpLinesForward, unsigned int column, bool optional)
{
	setKey(key);
	setComparator(comparator);
	setColumn(column);
	this->jumpLinesForwward = (jumpLinesForward);
	this->optional = optional;
}

string Token::getKey()
{
	return key;
}

Token& Token::setKey(string key)
{
	this->key = key;
	return *this;
}

string Token::getComparator()
{
	return comparator;
}

Token& Token::setComparator(string comparator)
{
	this->comparator = comparator;
	return *this;
}

unsigned int Token::getLinesForwward()
{
	return jumpLinesForwward;
}

unsigned int Token::getLine()
{
	return line;
}

Token& Token::setLine(unsigned int line)
{
	this->line = line;
	return *this;
}

unsigned int Token::getColumn()
{
	return column;
}

Token& Token::setColumn(unsigned int column)
{
	this->column = column;
	return *this;
}
