#ifndef ELECTRICITY_ENERGY_CONSUMPTION_INVOICE_H
#define ELECTRICITY_ENERGY_CONSUMPTION_INVOICE_H

#include <string>
#include <vector>

#include "Client.h"
#include "MonthlyConsumption.h"
#include "Constants.h"

using namespace std;

class Invoice {

public:
	static constexpr int HISTORY_MONTHS_COUNT = 13;
	
	Invoice& setClient(Client client);

	Client& getClient();

	Invoice& setTotalValue(double totalValue);

	double getTotalValue();

	Invoice& setMunicipalLightingContribution(double contribution);

	Invoice& setMunicipalLightingContribution(string contribution);

	double getMunicipalLightingContribution();

	Invoice& setElectricityPrice(double price);

	double getElectricityPrice();

	Invoice& setYellowFlag(double yellowFlag);

	double getYellowFlag();

	Invoice& setRedFlag(double redFlag);

	double getRedFlag();

	double getPreviousRead();

	Invoice& setPreviousRead(double value);

	Invoice& setActualRead(double value);

	double getActualRead();

	Invoice& setKwhConsumption(double consumption);

	double getKwhConsumption();

	Invoice& setInstallationNumber(string number);

	string getInstallationNumber();

	Invoice& setRegarding(string regarding);

	string getRegarding();

	Invoice& setDueDate(string dueDate);

	string getDueDate();

	Invoice& setActualReadingDate(string date);

	string getActualReadingDate();

	Invoice& setPreviousReadingDate(string date);

	string getPreviousReadingDate();

	Invoice& setNextReadingDate(string dueDate);

	string getNextReadingDate();

	const vector<MonthlyConsumption>& getConsumptionHistory();

	string toString();

	void createComsumptionHistory();

	bool isConsumptionEntryComplete();

	bool addConsumptionEntry(MonthlyConsumption consumption);

	bool containsHistoryFromMonthAndYear(int year, string month);

	MonthlyConsumption getCurrentMontlyConsumption();

private:
	Client client;

	double totalValue;
	double municipalLightingContribution;
	double electricityPrice;
	double redFlag;
	double yellowFlag;
	double previousRead;
	double actualRead;

	double kwhConsumption;

	string installationNumber;
	string regarding;
	string dueDate; //TODO verify which type is most appropriate
	string actualReadingDate;
	string previousReadingDate;
	string nextReadingDate;

	vector<MonthlyConsumption> consumptionHistory;
};

#endif //ELECTRICITY_ENERGY_CONSUMPTION_INVOICE_H