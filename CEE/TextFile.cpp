#include <codecvt>
#include "TextFile.h"
#include "Stringutils.h"


TextFile::TextFile(string fileName, enum class FileOpenMode mode) {
	this->name = fileName;

	switch (mode) {
	case FileOpenMode::APPEND:
		open(fileName, ios::app);
		break;
	case FileOpenMode::WRITE:
		open(fileName, ios::out);
		break;
	case FileOpenMode::READ:
		open(fileName, ios::in);
		break;
	default:
		break;
	}
}

vector<string> TextFile::getLines() {
	vector<string> content;
	string line;

	while (std::getline(*this, line)) {
		content.push_back(line);
	}

	return content;
}

string TextFile::getContent() {
	string buffer;
	unsigned int size;

	seekg(0, std::ios::end);
	size = static_cast<unsigned int>(tellg());

	buffer = string(size, ' ');

	seekg(0);
	(*this).read(&buffer[0], size);

	return buffer;
}

unsigned int TextFile::getSize() {
	seekp(0, std::ios::end);

	return static_cast<unsigned int>(tellg());
}

string TextFile::getName() {
	return name;
}