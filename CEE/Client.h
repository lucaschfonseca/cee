#ifndef ELECTRICITY_ENERGY_CONSUMPTION_CLIENT_H
#define ELECTRICITY_ENERGY_CONSUMPTION_CLIENT_H

#include <string>
#include <vector>

#include "HomeAddress.h"
#include "Constants.h"

using namespace std;

class Client {

public:	
	static const unsigned int MAXIMUM_INSTALLATIONS_PER_COSTUMER = 10;

	Client& setName(string name);

	string getName();

	string getCapitalizedName();

	Client& setCpf(string documentNumber);

	string getCpf();

	Client& setNumber(string number);

	string getNumber();

	bool addInstallation(string installationNumber);

	vector<string> getInstallations();

	Client& setHomeAddress(HomeAddress adress);

	HomeAddress& getHomeAddress();

	string toString();

	bool containsInstallation(string facilityNumber);

private:
	string name;
	string cpf;
	string number;

	vector<string>installations;

	HomeAddress homeAddress;
};

#endif //ELECTRICITY_ENERGY_CONSUMPTION_CLIENT_H
