#ifndef CONSUMPTION_H
#define CONSUMPTION_H

#include <string>

using namespace std;

class ConsumptionItem
{
public:
	string getDescription();

	ConsumptionItem& setDescription(string description);

	double getWatts();

	ConsumptionItem& setWatts(double watts);

	double getHours();

	ConsumptionItem& setHours(double hours);

	double getKwh();

	ConsumptionItem& setKwh(double kwh);

	string toString();

private:
	string description;

	double watts = 0;
	double hours = 0;
	double kWh = 0;
};

#endif // !define CONSUMPTION_H

