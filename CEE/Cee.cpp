#include <iostream>
#include <map>
#include <string>

#include "Cee.h"
#include "ConsumptionFileParser.h"
#include "DirectoryUtils.h"
#include "InvoiceParser.h"
#include "StringUtils.h"
#include "TextFile.h"

using namespace std;

Cee::Cee()
{
	filesOpenedCorrectly = fClient.isGood() && fInvoice.isGood();
}

ExecutionResult Cee::execute(int argc, char** argv)
{
	ExecutionResult result(ExecutionResultCode::Success);

	if (!filesOpenedCorrectly)
		result = ExecutionResult(ExecutionResultCode::UnableToOpenPersistenceFiles);
	else {
		switch (argc)
		{
		case 2:
			result = execute(argv[1]);
			break;
		case 3:
			result = execute(argv[1], argv[2]);
			break;
		case 4:
			result = execute(argv[1], argv[2], argv[3]);
			break;
		default:
			result = ExecutionResult(ExecutionResultCode::IncorrectSyntax);
			break;
		}
	}

	if (!result.isSuccess())
		cout << result.getErrorMessage();

	cout << endl;

	return result;
}

ExecutionResult Cee::execute(const char* arg)
{
	string str = string(arg);

	if (StringUtils::isOnlyDigit(str))
		return consumerResearch(str);

	return importInvoices(str);
}

ExecutionResult Cee::execute(const char* arg1, const char* arg2)
{
	string str = string(arg1);

	if (StringUtils::equalsIgnoreCase(arg1, PARAMETER_TXT_IMPORT))
		return importInvoices(arg2);

	if (StringUtils::isOnlyDigit(arg1))
		return consumerResearch(str, arg2);

	return ExecutionResult(ExecutionResultCode::Success);
}

ExecutionResult Cee::execute(const char* arg1, const char* arg2, const char* arg3)
{
	if (!StringUtils::endsWithIgnoreCase(arg3, EXT_TXT))
		return consumerResearch(arg1, arg2, arg3);

	return calculateConsumption(arg1, arg2, arg3);

	return ExecutionResult(ExecutionResultCode::Success);
}

#pragma region Invoice import

ExecutionResult Cee::importInvoices(const string& path)
{
	InvoiceParser parser;

	vector<string> imported;
	vector<string> failures;
	vector<string> alreadyImported;

	Invoice aux;

	map<string, int> conversionErrors;

	auto convertedInvoices = convertInvoice(path, conversionErrors);

	if (convertedInvoices.empty() && conversionErrors.empty())
		return ExecutionResult(ExecutionResultCode::NoInvoicesFound);

	for (string currentImporting : convertedInvoices) {
		TextFile file(currentImporting, FileOpenMode::READ);
		Client client;
		Invoice invoice;
		auto content = file.getLines();

		if (parser.parse(invoice, content)) {
			if (fInvoice.find<string, string>(aux, invoice.getInstallationNumber(), invoice.getRegarding(),
				fInvoice.compareInstallationRegarding) == -1) {

				fInvoice.writeInto(invoice);
				int clientPos = fClient.find<const string>(client, invoice.getClient().getNumber(), fClient.compareNumbers);

				if (clientPos == -1)
					fClient.writeInto(invoice.getClient());
				else {
					string currentInstallation = invoice.getClient().getInstallations().at(0);

					if (!client.containsInstallation(currentInstallation)) {
						client.addInstallation(currentInstallation);

						fClient.removeAt(clientPos);
						fClient.writeInto(client);
					}
				}

				imported.emplace_back(currentImporting);
			}
			else
				alreadyImported.emplace_back(currentImporting);
		}
		else
			failures.emplace_back(currentImporting);

		file.close();

		//remove(currentImporting.c_str()); // erases the temporary text file
	}

	showImportResult(imported, alreadyImported, failures, conversionErrors);

	if (!failures.empty())
		return ExecutionResult(ExecutionResultCode::ImportationError);

	return ExecutionResult(ExecutionResultCode::Success);
}

vector<string> Cee::convertInvoice(const string& path, map<string, int>& convertError)
{
	vector<string>converted;

	convertError.clear();

	if (StringUtils::endsWithIgnoreCase(path, EXT_TXT))
		converted.emplace_back(path);
	else if (DirectoryUtils::createIfNotExists(TEMP_INVOICE_DIR))
	{
		if (StringUtils::endsWithIgnoreCase(path, EXT_PDF)) {
			string target;

			if (StringUtils::contains(path, "\\"))
				target = path.substr(path.find_last_of("\\") + 1);
			else
				target = string(path);

			target = StringUtils::replaceAll(target, EXT_PDF, EXT_TXT);

			string converteInvoicePath = string(TEMP_INVOICE_DIR).append("\\").append(target);
			string converterCommand = string(CONVERT_FROM_PDF_TO_TEXT).append(" ").append(path).append(" ").append(converteInvoicePath);

			int coversionResult = system(converterCommand.c_str());
			if (coversionResult == PDF_CONVERT_SUCCESS)
				converted.emplace_back(converteInvoicePath);
			else
				convertError.insert(pair<string, int>(path, coversionResult));
		}
		else if (DirectoryUtils::exists(path))
		{
			vector <string> files;
			vector <string> currentConvert;

			DirectoryUtils::getAllFiles(path, files, EXT_PDF);

			for (auto s : files) {
				if (!(currentConvert = convertInvoice(s, convertError)).empty())
					for (auto conv : currentConvert)
						converted.emplace_back(conv);
			}
		}
	}

	return converted;
}

void Cee::showImportResult(vector<string> imported, vector<string> alreadyImported, vector<string> failures,
	const map<string, int>& conversionError)
{
	cout << INFO << imported.size() << " " << INVOICE << "(s) " << IMPORTED << "(s)" << endl;
	if (!imported.empty()) {
		cout << endl;
		for (auto s : imported)
			cout << "\t -> " << s << endl;
	}

	if (!alreadyImported.empty()) {
		cout << endl << INFO << alreadyImported.size() << " " << INVOICE << "(s) " << ALREADY_IMPORTED << endl << endl;

		for (auto s : alreadyImported)
			cout << "\t -> " << s << endl;
	}

	if (!failures.empty()) {
		cout << endl << ERR << failures.size() << " " << IMPORT_FAILURES << endl << endl;
		for (auto s : failures)
			cout << "\t -> " << s << endl;

		cout << endl;
	}

	if (!conversionError.empty()) {
		cout << endl << ERR << conversionError.size() << " " << CONVERSION_FAILURES << endl;
		for (auto it = conversionError.begin(); it != conversionError.end(); it++)
		{
			cout << endl << "\t -> " << it->first << ": ";

			switch (it->second)
			{
			case PDF_CONVERT_OPEN_FAILURE:
				cout << FAILED_TO_OPEN_PDF;
				break;
			case PDF_CONVERT_OUTPUT_OPEN_FAILURE:
				cout << FAILED_TO_OPEN_OUTPUT_FILE;
				break;
			case PDF_CONVERT_PERMISSIONS_FAILURE:
				cout << PERMISSION_ERROR;
				break;
			default:
				cout << UNKNOWN_ERROR;
				break;
			}
		}

		cout << endl;
	}
}

#pragma endregion

#pragma region Consumer research
ExecutionResult Cee::consumerResearch(const string& arg1, const string& arg2, const string& arg3)
{
	Client client;
	vector<Invoice>invoices;

	int clientAt = fClient.find<const string>(client, arg1, fClient.compareNumbers);

	if (clientAt == -1)
		return ExecutionResult(ExecutionResultCode::ClientNotFound);

	if (arg2.empty())
		invoices = fInvoice.findAll<const string>(arg1, fInvoice.compareClientNumbers);
	else if (arg3.empty())
		invoices = fInvoice.findAll<const string, const string>(arg1, arg2, fInvoice.compareClientAndRegarding);
	else
		invoices = fInvoice.findAll2<const string&>(arg1, arg2, arg3, fInvoice.compareClientAndPeriod);

	if (invoices.empty())
		return ExecutionResult(ExecutionResultCode::NoInvoicesFound);

	cout << K_CONSUMPTION_HISTORY << " - " << StringUtils::capitalizeWords(invoices.at(0).getClient().getName());

	if (arg3.empty())
	{
		for (auto invoice : invoices)
			showConsumptionHitoryData(invoice);
	}
	else
		showPeriodicalConsumption(invoices, arg2, arg3);

	return ExecutionResult(ExecutionResultCode::Success);
}

void Cee::showConsumptionHitoryData(Invoice invoice)
{
	cout << endl << endl
		<< StringUtils::capitalizeWords(K_INSTALLATION_NUMBER) << ": " << invoice.getInstallationNumber() << endl
		<< StringUtils::capitalizeWords(K_REGARDING) << ": " << invoice.getRegarding().append("\n")
		<< StringUtils::capitalizeWords(K_DUE_DATE) << ": " << invoice.getDueDate().append("\n")
		<< StringUtils::capitalizeWords(K_CONSUMPTION_KWH) << ": " << invoice.getKwhConsumption() << endl
		<< StringUtils::capitalizeWords(K_AMOUNT_PAYABLE) << ": " << MONEY_SIGN << " " << invoice.getTotalValue()
		;
}

bool compareDate(MonthlyConsumption consumption, MonthlyConsumption another)
{
	int year = consumption.getYear();
	int anotherYear = another.getYear();

	if (year < anotherYear)
		return true;

	if (year > anotherYear)
		return false;

	if (consumption.getIntegerMonth() < another.getIntegerMonth())
		return true;

	return false;
}

void Cee::showPeriodicalConsumption(const vector<Invoice>& invoices, string periodBegin, string periodEnd)
{
	vector<MonthlyConsumption> items;
	MonthlyConsumption beginConsumption;
	MonthlyConsumption endConsumption;

	beginConsumption.setMonth(periodBegin.substr(0, periodBegin.find("/")));
	beginConsumption.setYear(StringUtils::toInt(periodBegin.substr(periodBegin.find_first_not_of(beginConsumption.getMonth()) + 1)));
	beginConsumption.formatYearAsTwoPlaces();

	endConsumption.setMonth(periodEnd.substr(0, periodEnd.find("/")));
	endConsumption.setYear(StringUtils::toInt(periodEnd.substr(periodEnd.find_first_not_of(endConsumption.getMonth()) + 1)));
	endConsumption.formatYearAsTwoPlaces();

	for (auto invoice : invoices) {
		auto currentHistory = invoice.getConsumptionHistory();

		if (!currentHistory.empty())
			items.insert(items.end(), currentHistory.begin(), currentHistory.end());
	}

	sort(items.begin(), items.end(), compareDate);

	map<string, MonthlyConsumption> printed;

	cout << endl;
	for (auto c : items) {
		if ((c.refersSameMonth(beginConsumption) || c.refersSameMonth(endConsumption)) || (c.isBefore(endConsumption) && c.isAfter(beginConsumption))) {
			if (printed.find(c.getJoinedMonthYear()) == printed.end())
			{
				cout << endl << K_REGARDING << " " << c.getJoinedMonthYear() << "\n" << c.toString();
				printed.insert(pair<string, MonthlyConsumption>(c.getJoinedMonthYear(), c));
			}
		}
	}
}

#pragma endregion

#pragma region Consumption calculation
ExecutionResult Cee::calculateConsumption(const string& clientNumber, const string& regarding, const string& inputPath)
{
	Client client;
	Invoice invoice;

	int clientAt;
	int invoiceAt;

	TextFile file(inputPath, FileOpenMode::READ);
	if (!file.is_open())
		return ExecutionResult(ExecutionResultCode::UnableToOpenFile);

	// retrieves client
	clientAt = fClient.find<const string>(client, clientNumber, fClient.compareNumbers);
	if (clientAt == -1)
		return ExecutionResult(ExecutionResultCode::ClientNotFound);

	// retrieves invoice
	invoiceAt = fInvoice.find<const string, const string>(invoice, clientNumber, regarding, fInvoice.compareClientAndRegarding);
	if (invoiceAt == -1)
		return ExecutionResult(ExecutionResultCode::NoInvoicesFound);

	// parse and check the input
	vector<ConsumptionItem> consumptionItems = ConsumptionFileParser().parse(file.getLines());
	file.close();
	if (consumptionItems.empty())
		return ExecutionResult(ExecutionResultCode::InvalidInput);

	MonthlyConsumption monthlyConsumption = invoice.getCurrentMontlyConsumption();
	MonthlyConsumption requestedConsumption = recoverConsumptionInfo(consumptionItems, monthlyConsumption.getDaysCount());

	double consum = requestedConsumption.getKwh() * invoice.getElectricityPrice();
	consum += invoice.getRedFlag() + invoice.getYellowFlag() + invoice.getMunicipalLightingContribution();

	cout << CEMIG_INVOICE_DATA << " " << regarding << endl << monthlyConsumption.toString()
		<< VALUE_OF_ELECTRIC_CONSUMPTION << StringUtils::toStringWithPrecision(invoice.getTotalValue());

	cout << "\n\n" << DATA_CALCULATED_FOR << " " << regarding << endl << requestedConsumption.toString()
		<< VALUE_OF_ELECTRIC_CONSUMPTION << StringUtils::toStringWithPrecision(consum, 2);

	return ExecutionResult(ExecutionResultCode::Success);
}

MonthlyConsumption Cee::recoverConsumptionInfo(vector<ConsumptionItem>& consumptionItems, int days)
{
	MonthlyConsumption monthlyConsumption;
	double kwh = 0;

	for (ConsumptionItem item : consumptionItems) {
		kwh += item.getKwh();
	}

	monthlyConsumption.setAverageKWhPerDay(kwh / days);
	monthlyConsumption.setDaysCount(days);
	monthlyConsumption.setKwh(kwh);

	return monthlyConsumption;
}
#pragma endregion