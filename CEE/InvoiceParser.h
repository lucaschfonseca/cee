#ifndef INVOICE_PARSER_H
#define INVOICE_PARSER_H

#include <string>
#include <vector>
#include <regex>
#include <map>

#include "Invoice.h"
#include "Token.h"
#include "Parser.h"


using namespace std;

class InvoiceParser : Parser
{
public:

	/**
	 * Convert a string vector to an invoice object.
	 *
	 * @param invoice reference of a variable where the parsed invoice wil be stored.
	 * @param content the content to be parsed
	 * @return true if success false if fail.
	 */
	bool parse(Invoice& invoice, vector<string> content);
private:

	map<string, Token> fetchTokensFromContent(vector<string> content, map<string, Token> &targets);

	/**
	 * parses the invoice consumption history.
	 *
	 * @param invoice the invoice.
	 * @param content the historic source.
	 * @param beginsAt the parse starting point.
	 * @return true if success, false if fail;
	 */
	bool parseHistoric(Invoice& invoice, vector<string>& content, unsigned int beginsAt);
};

#endif // !INVOICE_PARSER_H
