#ifndef ELECTRICITY_ENERGY_CONSUMPTION_CONSUMPTION_H
#define ELECTRICITY_ENERGY_CONSUMPTION_CONSUMPTION_H

#include <string>
#include <map>

#include "Constants.h"

using namespace std;

class MonthlyConsumption {

public:
	MonthlyConsumption();

	MonthlyConsumption(string month, int year);

	string getMonth();

	MonthlyConsumption& setMonth(string month);

	int getYear();

	MonthlyConsumption& setYear(int year);

	double getKwh();

	MonthlyConsumption& setKwh(double kwh);

	int getDaysCount();

	MonthlyConsumption& setDaysCount(int days);

	double getAverageKWhPerDay();

	MonthlyConsumption& setAverageKWhPerDay(double averageKWhPerDay);

	string getJoinedMonthYear();

	string toString();

	int getIntegerMonth();

	void formatYearAsTwoPlaces();

	bool isBefore(MonthlyConsumption another);

	bool isAfter(MonthlyConsumption another);

	bool refersSameMonth(MonthlyConsumption another);

private:

	map<string, int> months = {
		{K_JAN, 1},
		{K_FEB, 2},
		{K_MAR, 3},
		{K_ABR, 4},
		{K_MAI, 5},
		{K_JUN, 6},
		{K_JUL, 7},
		{K_AGO, 8},
		{K_SET, 9},
		{K_OUT, 10},
		{K_NOV, 11},
		{K_DEZ, 12}
	};

	string month;

	int year;
	int daysCount;

	double kwh;
	double averageKWhPerDay;
};

#endif //ELECTRICITY_ENERGY_CONSUMPTION_CONSUMPTION_H
